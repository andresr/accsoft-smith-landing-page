package main;

use nginx;

sub htmlContent {
  my $hostname = `hostname`;
  my $uptime = `uptime -p`;
  my $nginxVersion = `nginx -v`;
  my $html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">
	   <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\">
	   <head>
	      <title>Nginx HTTP Server</title>
	      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
	      <style type=\"text/css\">
		 /*<![CDATA[*/
		    #link_bar a {
		            color:#ffffff;
		            text-align: center;
		            padding:15px;
		            width: 100px;
		            font-weight:bold;
		            text-decoration: none;
		            display: inline-block;
		    }
		    #link_bar a:link {
		            background-color:#3C6EB4;
		    }
		    #link_bar a:hover {
		            background-color:#294172;
		    }
		    body {
		            background-color: #fff;
		            color: #000;
		            font-size: 0.9em;
		            font-family: sans-serif,helvetica;
		            margin: 0;
		            padding: 0;
		    }
		    h1 {
		             text-align: center;
		             margin: 0;
		             padding: 0.6em 2em 0.4em;
		             background-color: #294172;
		             color: #fff;
		             font-weight: normal;
		             font-size: 1.75em;
		             border-bottom: 2px solid #000;
		    }
		    h1 strong {
		            font-weight: bold;
		            font-size: 1.5em;
		    }
		    h2 {
		            text-align: center;
		            background-color: #3C6EB4;
		            font-size: 1.1em;
		            font-weight: bold;
		            color: #fff;
		            margin: 0;
		            padding: 0.5em;
		            border-bottom: 2px solid #294172;
		    }
		    .content {
		            padding: 1em 4em;
		    }
		    .box_content {
		            border: 2px solid #000;
		    }
		    img {
		            border: 2px solid #fff;
		            padding: 2px;
		            margin: 2px;
		    }
		    a:hover img {
		            border: 2px solid #3C6EB4;
		    }
		    .logos {
		            margin: 1em;
		            text-align: center;
		    }
		    /*]]>*/
	      </style>
	   </head>
	   <body>
	      <h1><strong>${hostname}</strong></h1>
	      <h2>${uptime}</h2>
	      <div class=\"content\">
		 <div class=\"box_content\">
		    <h2>Modules</h2>
		    <div class=\"content\">
		       <div id=\"link_bar\">
		          <p><a href=\"/cmx\">CMX Metrics</a></p>
		       </div>
		    </div>
		 </div>
	      </div>
	      <div class=\"logos\">
		 <p>${nginxVersion}</p>
	      </div>
	   </body>
	 </html>";
  
  return $html;
}

sub handler {
    my $r = shift;
    $r->send_http_header("text/html");
    return OK if $r->header_only;
    
    $r->print(htmlContent());
    return OK;
}

1;
__END__
